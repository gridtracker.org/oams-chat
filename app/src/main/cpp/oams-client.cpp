//
// Created by mchambers on 5/2/21.
//
#define ASIO_STANDALONE

#include <cstdlib>
#include <iostream>
#include <string>
#include <websocketpp/client.hpp>
#include <websocketpp/config/asio_no_tls_client.hpp>
#include <json.hpp>

typedef websocketpp::client<websocketpp::config::asio_client> client;

using websocketpp::lib::placeholders::_1;
using websocketpp::lib::placeholders::_2;
using websocketpp::lib::bind;

typedef websocketpp::config::asio_client::message_type::ptr message_ptr;

using json = nlohmann::json;

class oamsClient {
public:
    client g_gtChatSocket;
    int g_gtEngineInterval;
    int g_gtStatusCount = 0;
    int g_gtStatusTime = 500;
    int g_gtMaxChatMessages = 100;
    bool g_gtNeedUsersList = true;
    bool g_gtLiveStatusUpdate = false;
    long long myChatId = 0;
    long long myRoom = 0;
    int g_gtChatListChangeCount = 0;
    int g_gtCurrentMessageCount = 0;

    enum ChatState {
        none = -1,
        idle = 0,
        //connect = 1,
        connecting = 2,
        connected = 3,
        status = 4,
        closed = 5,
        error = 6,
    };

    class oams_mesg {
    public:
        std::string type;
        std::string mesg;

        oams_mesg(json rawMesg) {
            // constructor using json class to break incoming payloads down to their parts
        }
    };

    ChatState g_gtState = none;

    void gtConnectChat() {
        if (false) {
            // we should start over
            g_gtState = error;
            return;
        }
        int rnd = rand() % 10 + 18360;
        std::string uri = "ws://kube1.mchambersradio.com:" + std::to_string(rnd);
        try {
            g_gtState = connecting;
            g_gtChatSocket.init_asio();
            g_gtChatSocket.set_message_handler(bind(&gtChatMessage,&g_gtChatSocket,::_1,::_2));
            websocketpp::lib::error_code ec;
            client::connection_ptr con = g_gtChatSocket.get_connection(uri, ec);
            if (ec) {
                g_gtState = error;
                return;
            }
            g_gtChatSocket.connect(con);
            g_gtChatSocket.run();
        } catch (websocketpp::exception const &e) {
            g_gtState = error;
            return;
        }
        g_gtState = connected;
    }
    void gtChatMessage(client* c, websocketpp::connection_hdl hdl, message_ptr msg) {
        oams_mesg newMesg(msg->get_payload());
        // code here to do appropriate stuff with incoming message
    }

    // need functions to handle populating list of who's online, send "login", set UUID, send mesg, update status

    // need
};